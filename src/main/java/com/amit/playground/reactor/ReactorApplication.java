package com.amit.playground.reactor;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class ReactorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactorApplication.class, args);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    @Bean
    public CommandLineRunner demo() {
        return (args) -> {
//            Flux f  = Flux.generate(sink-> {
//                double random = Math.random();
//                if(random>0.5) {
//                    sink.next(Mono.empty());
//                }else {
//                    sink.next(Mono.just(random));
//                }
//            }).switchIfEmpty(repeat->Mono.delay(Duration.ofSeconds(1000)));
//
//            f.log().subscribe();
//            return;

            //TODO: instead of using generate consider using create:
            //https://www.baeldung.com/flux-sequences-reactor
            //this way an external source e.g. a timer or request to execute can generate the
            //eventstream
            System.out.println("Master thread"+ "-" + Thread.currentThread().getName());
            //Test.poll().doOnNext(System.out::println).blockLast();
            Flux g = Flux.generate((sink) -> {
                //At most one next(T) call and/or one complete() or error(Throwable) should be called per invocation of the generator function.

                        //synchronous generate
                        System.out.println("generate"+ "-" + Thread.currentThread().getName());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //if the db is empty then dont pass on value
                        Double rand = Math.random();
                        if(rand>0.1){
                            sink.next(Optional.of(rand));
                        }else{
                            //no new queue elements to process atm
                            //System.out.println("Nothing to process block downstream");
                            sink.next(Optional.empty());
                        }


                        //we setup a timer, if the timer elapses, we should requery the db for new rows
//                        Mono.delay( Duration.ofMillis(2000) )
//                            .doOnSubscribe(System.out::println)
//                            .subscribe(System.out::println);

                //we maybe able to achieve this the state variable.  The consumer can update the state
                //of the flux
                    });



                    ;//.sampleTimeout((d) -> Mono.delay(Duration.ofMillis(1000)));
            //publishOn: Typically used for fast publisher, slow consumer(s) scenarios.
                    //.publishOn(Schedulers.parallel());//.delayUntil(it->Mono.just(1).delayElement(Duration.ofSeconds(500)));




            //subscribeOn: Typically used for slow publisher e.g., blocking IO, fast consumer(s) scenarios.
            //Note that if you are using an eager or blocking create(Consumer, FluxSink.OverflowStrategy) as the source, it can lead to deadlocks due to requests piling up behind the emitter. In such case, you should call subscribeOn(scheduler, false) instead.
            g
                     .subscribeOn(Schedulers.boundedElastic(),true)
                    .log()
                    .subscribe(new BaseSubscriber<Optional<Double>>() {
                        @Override
                        public void hookOnSubscribe(Subscription subscription) {
                            request(1);
                        }
                @Override
                protected void hookOnNext(Optional<Double> value){

                        //new row came in
                        if(value.isPresent()) {
                            //process rows as fast as possible if there are jobs in the queue

                            Mono<Object> mono2  = Mono.fromCallable(() -> {
                                request(1); //request next element
                                System.out.println(value.get() + "-" + Thread.currentThread().getName());
                                try {
                                    Thread.sleep((long)  (value.get() * 1000));
                                }catch (InterruptedException e){
                                    System.out.println("somebody timed us out");
                                }
                                System.out.println(value.get() + "-" + Thread.currentThread().getName());

                                return null;
                            })
                                    //.block(Duration.ofMillis(1000));
                            .timeout(Duration.ofMillis(900)).onErrorResume((throwable) -> {
                                        System.out.println("timeout error");
                                //request(1); //request next element
                                return Mono.empty();
                            })
                                    .subscribeOn(Schedulers.boundedElastic());
                           mono2.subscribe();
                                    //Recall: this is executed on boundedElastic scheduler
                                    //.block();

                            //no new row came in to proces
                        }else{
                            //wait and stop this sequence generation until a timeout
                            System.out.println("nothing to process, wait and get - " + Thread.currentThread().getName());
                            Mono.delay(Duration.ofMillis(4000)).doOnNext(t->request(1)).block();
                        }

                       //if value is empty, delay x seconds and request next element;

//                        if (value == 5) {
//                            cancel();
//                        }

                }

            });


//            Flux.interval(Duration.ofMillis(100))
//                    .subscribe(new BaseSubscriber<Object>(){
//                        @Override
//                        protected void hookOnNext(Object value){
//                            System.out.println(value+"-"+ Thread.currentThread().getName());
//                            g.next();
//                        }
//                    });
//
        };

    }

}
