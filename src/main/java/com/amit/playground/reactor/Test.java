package com.amit.playground.reactor;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Date;

public class Test {

    public Test(){

    }

    static Flux poll(){
        //unlike interval, delay sequence waits time between consecutive calls in case
        //calls take variable times to finish
        return Flux.generate(sink -> {

                sink.next(Math.random());
            }
        )
                .delayElements( Duration.ofSeconds(1));//calls sink ever 100 msecs

    }




}
